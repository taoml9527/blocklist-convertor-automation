# BlockList Convertor Automation

Automation for converting ABP filters to Apple format Blocking Rules.

Automation process integrates the necessary components and is executed in GitLab CI environment based on a well defined pipeline.

# Building & Testing

- It is required to have swift installed for usage from the terminal. Running `xcode-select --install` should take care of it.
- To setup the automation to be used, just execute `make` in your terminal.
- Testing conversion:
	- Create an input `<filename>.txt` file in `Storage/MergedFilters` folder.
	- Execute `swift run blocklist-convertor-automation convert <filename> <safari version>`.
	- The output is written to `Storage/ConvertedBlockingRules/<filename>.json`
	- Example:
		- `myTestFile.txt` with some filters is added to `Storage/MergedFilters`.
		- `swift run blocklist-convertor-automation convert myTestFile 15.0` is executed.
		- Check the content of `myTestFile.json` in `Storage/ConvertedBlockingRules`

# Components

- blocklist-convertor-automation command line executable is the middleware which integrates all components.
- ABP filter endpoints are used to download the source filters meant to be converted to Apple rules format.
- abp2blocklist is used to converted a list of ABP filters to Apple rules format. Handles only request blocking filters.
- FilterParser is used to convert element hiding ABP filters to Apple rules format. Eventually it should replace abp2blocklist.
- Converted rules are deployed to contentblockerlists, so those can go live.

# Automation Pipeline

### 1. Filters download

This is the initial step of automation. In this step the source filters are downloaded from the endpoints specified in [FilterSource](./Sources/blocklist-convertor-automation/FilterSource.swift).

### 2. Merge filters

In this step, base filters are merged to create the necesary filter list before conversion. The predefined merge combination is defined in [BlockList](./Sources/blocklist-convertor-automation/BlockList.swift) filterComposition.

### 3. Convert filters

The filters merged in previous step are converted by using abp2blocklist. The result of conversion is stored in Storage/ConvertedBlockingRules, and will be used in step 4 to perform the deploy.

### 4. Blocking Rules testing

It is hard, almost impossible, to test that the converted rules are actually blocking/allowing all of the defined resources in base filters. Therefore we trust that abp2blocklist produces the acceptable results in terms of content blocking quality.
Still, we at least should make sure that the generated rules can be compiled with WebKit, thus avoiding the potential of breaking completely the content blocking after a potential deploy.
Therefore, in this step, each of the generated rules set, is compiled with WebKit, making sure that there is no breaking rule.

### 5. Rules deploy

In this step the converted rules from step 3 are copied to contentblockerlist submodule. If after copying there are no rules changed, no deploy is happening; otherwise a push to contentblockerlist with timestamped commit is made, so the changes are deployed live.

### 6. State save

In This step, the last ABP filters , their merged variants and generated BlockingRules are saved in Storage folder. This will be useful to have a history of which base filters were used for particular deployed rules. Also, it may help with trubleshooting some possible content blocking failures that went live.

### Automation flow

![Automation flow](./AutomationFlow.png)

# Making Changes

### Adding a new Filter Source
1. Define the new filter source in [FilterSource](./Sources/blocklist-convertor-automation/FilterSource.swift). Add download downloadURL and storageFileName.
2. Add the freshly added FilterSource to target [BlockList](./Sources/blocklist-convertor-automation/BlockList.swift) filterComposition.

### Adding new Blocking List

1. Define the new blocking list in [BlockList](./Sources/blocklist-convertor-automation/BlockList.swift).
2. Create the appropriate filterComposition from the available filter sources, or consider adding new filter sources.
3. Add a test function to validate the new BlockList in RulesTester. The new BlockingRules json file needs to be added to tester bundle.
4. For new rules list to be available, a new endpoint should be set up - add the new rules file name in [filtermasterserver](https://gitlab.com/eyeo/devops/legacy/infrastructure/-/blob/master/hiera/roles/filtermasterserver.yaml#L20) and create a MR on [infra](https://gitlab.com/eyeo/devops/legacy/infrastructure) repo. Once approved, it will take 5-10 minutes for the rules to go live on a new endpoint

