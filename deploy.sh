#!/bin/bash

# Deploy the converted rules to contentblockerlists repo

cd contentblockerlists
git checkout master
git pull --rebase
echo "Copying generated rules to contentblockerlists"

# Explicetely copy converted files, this meant to assure that only intended files are copied
# to contentblockerlists.
# For example we don't want to override easylist_content_blocker and easylist+exceptionrules_content_blocker
# which are maintained by hand, and do not use abp2blocklist for conversion
CONVERTOR_STORAGE=../Storage/ConvertedBlockingRules
cp $CONVERTOR_STORAGE/easylist+abpvn-minified+exceptionrules-minimal.json \
   $CONVERTOR_STORAGE/easylist+abpvn-minified.json \
   $CONVERTOR_STORAGE/easylist+easylistchina-minified+exceptionrules-minimal.json \
   $CONVERTOR_STORAGE/easylist+easylistchina-minified.json \
   $CONVERTOR_STORAGE/easylist+easylistdutch-minified+exceptionrules-minimal.json \
   $CONVERTOR_STORAGE/easylist+easylistdutch-minified.json \
   $CONVERTOR_STORAGE/easylist+easylistgermany-minified+exceptionrules-minimal.json \
   $CONVERTOR_STORAGE/easylist+easylistgermany-minified.json \
   $CONVERTOR_STORAGE/easylist+easylistitaly-minified+exceptionrules-minimal.json \
   $CONVERTOR_STORAGE/easylist+easylistitaly-minified.json \
   $CONVERTOR_STORAGE/easylist+easylistspanish-minified+exceptionrules-minimal.json \
   $CONVERTOR_STORAGE/easylist+easylistspanish-minified.json \
   $CONVERTOR_STORAGE/easylist+liste_fr-minified+exceptionrules-minimal.json \
   $CONVERTOR_STORAGE/easylist+liste_fr-minified.json \
   $CONVERTOR_STORAGE/easylist+ruadlist-minified+exceptionrules-minimal.json \
   $CONVERTOR_STORAGE/easylist+ruadlist-minified.json \
   $CONVERTOR_STORAGE/easylist_min+exceptionrules_content_blocker.json \
   $CONVERTOR_STORAGE/easylist_min_content_blocker.json \
   $CONVERTOR_STORAGE/easylist+abpvn-minified+exceptionrules-minimal+safari15.json \
   $CONVERTOR_STORAGE/easylist+abpvn-minified+safari15.json \
   $CONVERTOR_STORAGE/easylist+easylistchina-minified+exceptionrules-minimal+safari15.json \
   $CONVERTOR_STORAGE/easylist+easylistchina-minified+safari15.json \
   $CONVERTOR_STORAGE/easylist+easylistdutch-minified+exceptionrules-minimal+safari15.json \
   $CONVERTOR_STORAGE/easylist+easylistdutch-minified+safari15.json \
   $CONVERTOR_STORAGE/easylist+easylistgermany-minified+exceptionrules-minimal+safari15.json \
   $CONVERTOR_STORAGE/easylist+easylistgermany-minified+safari15.json \
   $CONVERTOR_STORAGE/easylist+easylistitaly-minified+exceptionrules-minimal+safari15.json \
   $CONVERTOR_STORAGE/easylist+easylistitaly-minified+safari15.json \
   $CONVERTOR_STORAGE/easylist+easylistspanish-minified+exceptionrules-minimal+safari15.json \
   $CONVERTOR_STORAGE/easylist+easylistspanish-minified+safari15.json \
   $CONVERTOR_STORAGE/easylist+liste_fr-minified+exceptionrules-minimal+safari15.json \
   $CONVERTOR_STORAGE/easylist+liste_fr-minified+safari15.json \
   $CONVERTOR_STORAGE/easylist+ruadlist-minified+exceptionrules-minimal+safari15.json \
   $CONVERTOR_STORAGE/easylist+ruadlist-minified+safari15.json \
   $CONVERTOR_STORAGE/easylist_min+exceptionrules_content_blocker+safari15.json \
   $CONVERTOR_STORAGE/easylist_min_content_blocker+safari15.json \
   ./
      

if [ -z "$(git status --porcelain)" ]
then
    echo "No new rules were generated, no deploy will be made."
else
    CURRENTDATE="$(date)"
    git add .
    git status
    git commit -m "Auto updated rules at $CURRENTDATE"
    git remote set-url origin git@gitlab.com:eyeo/filterlists/contentblockerlists.git
    git push origin HEAD:master
fi
