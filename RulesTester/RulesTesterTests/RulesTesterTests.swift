import XCTest
import WebKit
@testable import RulesTester

class RulesTesterTests: XCTestCase {

    var wkStore: WKContentRuleListStore!

    enum Errors: Error {
        case missingRules
    }

    override func setUp() {
        super.setUp()

        guard let url = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first else {
            return XCTFail("Failed to create url for content rules store")
        }

        guard let store = WKContentRuleListStore(url: url) else {
            return XCTFail("Failed to create content rules store")
        }

        wkStore = store
    }

    func test_easyList() throws {
        try assert_that_rulesCanBeCompiled(forList: "easylist_min_content_blocker")
    }

    func test_easylistPlusExceptions() throws {
        try assert_that_rulesCanBeCompiled(forList: "easylist_min+exceptionrules_content_blocker")
    }

    func test_easylistChina() throws {
        try assert_that_rulesCanBeCompiled(forList: "easylist+easylistchina-minified")
    }

    func test_easylistChinaPlusExceptions() throws {
        try assert_that_rulesCanBeCompiled(forList: "easylist+easylistchina-minified+exceptionrules-minimal")
    }

    func test_easylistDutch() throws {
        try assert_that_rulesCanBeCompiled(forList: "easylist+easylistdutch-minified")
    }

    func test_easylistDutchPlusExceptions() throws {
        try assert_that_rulesCanBeCompiled(forList: "easylist+easylistdutch-minified+exceptionrules-minimal")
    }

    func test_easylistFrance() throws {
        try assert_that_rulesCanBeCompiled(forList: "easylist+liste_fr-minified")
    }

    func test_easylistFrancePlusExceptions() throws {
        try assert_that_rulesCanBeCompiled(forList: "easylist+liste_fr-minified+exceptionrules-minimal")
    }

    func test_easylistGermany() throws {
        try assert_that_rulesCanBeCompiled(forList: "easylist+easylistgermany-minified")
    }

    func test_easylistGermanyPlusExceptions() throws {
        try assert_that_rulesCanBeCompiled(forList: "easylist+easylistgermany-minified+exceptionrules-minimal")
    }

    func test_easylistItaly() throws {
        try assert_that_rulesCanBeCompiled(forList: "easylist+easylistitaly-minified")
    }

    func test_easylistItalyPlusExceptions() throws {
        try assert_that_rulesCanBeCompiled(forList: "easylist+easylistitaly-minified+exceptionrules-minimal")
    }

    func test_easylistRussia() throws {
        try assert_that_rulesCanBeCompiled(forList: "easylist+ruadlist-minified")
    }

    func test_easylistRussiaPlusExceptions() throws {
        try assert_that_rulesCanBeCompiled(forList: "easylist+ruadlist-minified+exceptionrules-minimal")
    }

    func test_easylistSpain() throws {
        try assert_that_rulesCanBeCompiled(forList: "easylist+easylistspanish-minified")
    }

    func test_easylistSpainPlusExceptions() throws {
        try assert_that_rulesCanBeCompiled(forList: "easylist+easylistspanish-minified+exceptionrules-minimal")
    }

    func test_easylistVietnam() throws {
        try assert_that_rulesCanBeCompiled(forList: "easylist+abpvn-minified")
    }

    func test_easylistVietnamPlusExceptions() throws {
        try assert_that_rulesCanBeCompiled(forList: "easylist+abpvn-minified+exceptionrules-minimal")
    }
    
    func test_easyList_safari15() throws {
        try assert_that_rulesCanBeCompiled(forList: "easylist_min_content_blocker+safari15")
    }

    func test_easylistPlusExceptions_safari15() throws {
        try assert_that_rulesCanBeCompiled(forList: "easylist_min+exceptionrules_content_blocker+safari15")
    }

    func test_easylistChina_safari15() throws {
        try assert_that_rulesCanBeCompiled(forList: "easylist+easylistchina-minified+safari15")
    }

    func test_easylistChinaPlusExceptions_safari15() throws {
        try assert_that_rulesCanBeCompiled(forList: "easylist+easylistchina-minified+exceptionrules-minimal+safari15")
    }

    func test_easylistDutch_safari15() throws {
        try assert_that_rulesCanBeCompiled(forList: "easylist+easylistdutch-minified+safari15")
    }

    func test_easylistDutchPlusExceptions_safari15() throws {
        try assert_that_rulesCanBeCompiled(forList: "easylist+easylistdutch-minified+exceptionrules-minimal+safari15")
    }

    func test_easylistFrance_safari15() throws {
        try assert_that_rulesCanBeCompiled(forList: "easylist+liste_fr-minified+safari15")
    }

    func test_easylistFrancePlusExceptions_safari15() throws {
        try assert_that_rulesCanBeCompiled(forList: "easylist+liste_fr-minified+exceptionrules-minimal+safari15")
    }

    func test_easylistGermany_safari15() throws {
        try assert_that_rulesCanBeCompiled(forList: "easylist+easylistgermany-minified+safari15")
    }

    func test_easylistGermanyPlusExceptions_safari15() throws {
        try assert_that_rulesCanBeCompiled(forList: "easylist+easylistgermany-minified+exceptionrules-minimal+safari15")
    }

    func test_easylistItaly_safari15() throws {
        try assert_that_rulesCanBeCompiled(forList: "easylist+easylistitaly-minified+safari15")
    }

    func test_easylistItalyPlusExceptions_safari15() throws {
        try assert_that_rulesCanBeCompiled(forList: "easylist+easylistitaly-minified+exceptionrules-minimal+safari15")
    }

    func test_easylistRussia_safari15() throws {
        try assert_that_rulesCanBeCompiled(forList: "easylist+ruadlist-minified+safari15")
    }

    func test_easylistRussiaPlusExceptions_safari15() throws {
        try assert_that_rulesCanBeCompiled(forList: "easylist+ruadlist-minified+exceptionrules-minimal+safari15")
    }

    func test_easylistSpain_safari15() throws {
        try assert_that_rulesCanBeCompiled(forList: "easylist+easylistspanish-minified+safari15")
    }

    func test_easylistSpainPlusExceptions_safari15() throws {
        try assert_that_rulesCanBeCompiled(forList: "easylist+easylistspanish-minified+exceptionrules-minimal+safari15")
    }

    func test_easylistVietnam_safari15() throws {
        try assert_that_rulesCanBeCompiled(forList: "easylist+abpvn-minified+safari15")
    }

    func test_easylistVietnamPlusExceptions_safari15() throws {
        try assert_that_rulesCanBeCompiled(forList: "easylist+abpvn-minified+exceptionrules-minimal+safari15")
    }


    // MARK: - Helpers

    func assert_that_rulesCanBeCompiled(forList name: String) throws {
        let rules = try loadRules(forName: name)
        let expectation = expectation(description: "Wait for compile")
        wkStore?.compileContentRuleList(forIdentifier: name,
                                        encodedContentRuleList: rules,
                                        completionHandler: { list, error in
                                            switch (list, error) {
                                            case (.some, nil):
                                                expectation.fulfill()
                                            case let (nil, error?):
                                                XCTFail("\(name) compile failed with \(error)")
                                            default:
                                                XCTFail("Invalid compile result for \(name)")
                                            }
                                        })
        wait(for: [expectation], timeout: 30.0)
    }

    func loadRules(forName name: String) throws -> String {
        guard let path = Bundle(for: RulesTesterTests.self).path(forResource: name, ofType: "json") else {
            throw Errors.missingRules
        }

        return try String(contentsOfFile: path)
    }
}
