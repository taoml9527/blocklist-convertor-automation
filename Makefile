.DEFAULT_GOAL := all

all:
	git submodule update --init
	cd abp2blocklist; npm install
	swift build -c release
