// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "blocklist-convertor-automation",
    platforms: [
        .macOS(.v11)
        ],
    dependencies: [
        .package(url: "https://gitlab.com/eyeo/adblockplus/apple/filterparser.git",
                 .revisionItem("ff78a70a90bc4980380830c4074c0fd0a049c662"))
    ],
    targets: [
        .target(
            name: "blocklist-convertor-automation",
            dependencies: [.product(name: "ContentBlockerRules",
                                    package: "FilterParser")]),
        .testTarget(
            name: "blocklist-convertor-automationTests",
            dependencies: ["blocklist-convertor-automation"]),
    ]
)
