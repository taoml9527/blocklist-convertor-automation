
enum ScriptError: Error {
    case wrongArgumentsCount
    case unknownCommand(String)
}

// MARK: - Setup environment
guard CommandLine.argc > 1 else {
    throw ScriptError.wrongArgumentsCount
}

let command = CommandLine.arguments[1]

switch command {
case "download":
    try downloadFilters()
case "merge":
    try mergeFilters()
case "convert_all":
    try convertToBlockingRules()
case "convert":
    let fileName = CommandLine.arguments[2]
    let safariVersion = CommandLine.arguments[3]
    try convert(fileName, safariVersion: .init(safariVersion))
default:
    throw ScriptError.unknownCommand("Available commands: download, merge, convert")
}

