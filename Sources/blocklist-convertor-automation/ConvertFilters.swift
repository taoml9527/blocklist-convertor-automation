import Foundation
import ContentBlockerRules

let workQueue = DispatchQueue.init(label: "workQueue", qos: .utility, attributes: .concurrent, autoreleaseFrequency: .workItem, target: nil)
let group = DispatchGroup()

enum FilterListConversionFailure: Error {
    case failedToConvertFilterList(name: String)
}

/// Convertst the filters to BlockingRules using abp2blocklist
/// Should be called after filters where downloaded and merged
func convertToBlockingRules() throws {
    print("ℹ️  Started conversion to BlockingRules")
    print("\n")
    try BlockList.allCases.forEach { blockList in
        var error: Error?
        for safariVersion in blockList.safariVersions {
            group.enter()
            workQueue.async {
                print("🔄 Converting filter list for \(blockList.storageFileName(safariVersion)) for safari version \(safariVersion.value)")
                do {
                    try convert(blockList.storageFileName(safariVersion), safariVersion: safariVersion)
                } catch (let e) {
                    error = e
                }
                group.leave()
            }
        }
        group.wait()
        if let error = error {
            throw error
        }
    }
}

func convert(_ fileName: String,
             safariVersion: Version) throws {
    print("🔄 Converting filter list for \(fileName) for safari version \(safariVersion.value)")
    
    let readFilePath = mergedFiltersFolders + "\(fileName).txt"
    let convertedFilePath = convertedBlockingRulesPath + "\(fileName).json"
    
    var requestBlockingRules = try convertRequestBlockingFilters(readFilePath, convertedFilePath, safariVersion: safariVersion)
    let elementHidingRules = try convertElementHidingFilters(readFilePath)

    // Combine the two lists
    var allRules = elementHidingRules
    // abp2blocklist will output an empty array string if no rules were converted
    if requestBlockingRules.count > 4 {
        allRules.removeLast()
        requestBlockingRules.removeFirst()
        allRules.append(contentsOf: ",\n")
        allRules.append(contentsOf: requestBlockingRules)
    }
    
    try allRules.write(to: URL(fileURLWithPath: convertedFilePath), atomically: true, encoding: .utf8)
    
    print("✅ Finished converting filter list for \(fileName) for safari version \(safariVersion.value)")
    print("\n")
}

/// Convert the given merged filter list to request BlockingRules format
/// Calls abp2blocklist with shell commands
/// Reads the merged filters from Storage/MergedFilters
/// Writes results to contentblockerlists
/// - Parameters
///     - inputFile: The name of the merged filter list
///     - outputFile: Where to write the result
/// - Throws: Any error during conversion process
/// - Returns: The converted filters in BlockingRules format
private func convertRequestBlockingFilters(_ inputFile: String, _ outputFile: String, safariVersion: Version) throws -> String {
    let convertStatus = shell("node abp2blocklist/abp2blocklist.js --safari-version \(safariVersion.value) < \(inputFile) > \(outputFile)")
    if convertStatus != 0 {
        throw FilterListConversionFailure.failedToConvertFilterList(name: inputFile)
    }
    return try String(contentsOf: URL(fileURLWithPath: outputFile), encoding: .utf8)
}

/// Convert the element hiding filters
/// - Parameter inputFile: The name of the merged filter list
/// - Throws: Error if failed to convert filters
/// - Returns: The converted filters in BlockingRules format
private func convertElementHidingFilters(_ inputFile: String) throws -> String {
    let filters = try String(contentsOf: URL(fileURLWithPath: inputFile), encoding: .utf8)
    let rules = try ContentBlockerRuleParser.parseFrom(rawFilters: filters)
    let encoder = JSONEncoder()
    encoder.outputFormatting = [.withoutEscapingSlashes, .prettyPrinted]
    let jsonData = try encoder.encode(rules)
    return String(data: jsonData, encoding: .utf8)!
}
