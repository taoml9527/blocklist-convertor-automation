import Foundation

enum FiltersMergingFailure: Error {
    case failedToCreateMergedFile(forPath: String)
    case failedToCreateFileWritter(forPath: String)
    case failedToCreateFileReader(forPath: String)
    case emptyFilterSourceFile
}

/// Merges the filters based on BlockList filter composition
/// Should be called after base filters were downloaded
func mergeFilters() throws {
    print("ℹ️  Started to merge filters")
    print("\n")
    try BlockList.allCases.forEach { blockList in
        for safariVersion in blockList.safariVersions {
            print("🔀 Compose \(blockList) from \(blockList.filterComposition.map { $0.storageFileName }) for safari version \(safariVersion.value)")
            try mergeFilters(blockList.filterComposition, into: "\(blockList.storageFileName(safariVersion))")
            print("✅ Completed filters merging for \(blockList) for safari version \(safariVersion.value)")
            print("\n")
        }
    }
}

// MARK: - IO

/// Creates a combined filter file that contains the content of base filters from which a BlockList is composed.
/// Does not alter the base filters content.
/// Reads the base filters from Storage/DownloadedFilters
/// Writes the result to Storage/MergedFilters
/// - Parameters:
///   - filters: The base filters to combine
///   - fileName: The combined file name
/// - Throws: Any error during merging
private func mergeFilters(_ filters: [FilterSource], into fileName: String) throws {
    let filtersCachePath = downloadedFiltersFolder
    let mergedFilePath = mergedFiltersFolders + "\(fileName).txt"

    let isOK = FileManager.default.createFile(atPath: mergedFilePath, contents: nil)
    guard isOK else {
        throw FiltersMergingFailure.failedToCreateMergedFile(forPath: mergedFilePath)
    }

    guard let writer = FileHandle(forWritingAtPath: mergedFilePath) else {
        throw FiltersMergingFailure.failedToCreateFileReader(forPath: mergedFilePath)
    }

    try filters.forEach { filter in
        let readPath = filtersCachePath + "\(filter.storageFileName).txt"
        guard let reader = FileHandle(forReadingAtPath: readPath) else {
            throw FiltersMergingFailure.failedToCreateFileReader(forPath: readPath)
        }
        guard var data = try reader.readToEnd(), data.count > 0 else {
            throw FiltersMergingFailure.emptyFilterSourceFile
        }

        // Start on new line for the next file content
        data.append("\n".data(using: .utf8)!)
        writer.write(data)

        try reader.close()
    }

    try writer.close()
}
