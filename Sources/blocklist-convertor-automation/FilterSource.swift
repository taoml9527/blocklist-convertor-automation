import Foundation

/// The base filter lists used to construct BlockLists
enum FilterSource: CaseIterable {
    // Eyeo lists
    case easylist
    case exceptions
    case easylistChina
    case easylistDutch
    case easylistFrance
    case easylistGermany
    case easylistItaly
    case easylistRussia
    case easylistSpain
    case easylistVietnam
    case germanyCompliance
    case antiCV

    // Partner lists
    case cocCoc
}

extension FilterSource {
    var downloadURL: URL {
        switch self {
        case .easylist:
            return URL(string: "https://easylist-downloads.adblockplus.org/easylist-minified.txt")!
        case .exceptions:
            return URL(string: "https://easylist-downloads.adblockplus.org/exceptionrules-minimal.txt")!
        case .easylistChina:
            return URL(string: "https://easylist-downloads.adblockplus.org/easylistchina-minified.txt")!
        case .easylistDutch:
            return URL(string: "https://easylist-downloads.adblockplus.org/easylistdutch-minified.txt")!
        case .easylistFrance:
            return URL(string: "https://easylist-downloads.adblockplus.org/liste_fr-minified.txt")!
        case .easylistGermany:
            return URL(string: "https://easylist-downloads.adblockplus.org/easylistgermany-minified.txt")!
        case .easylistItaly:
            return URL(string: "https://easylist-downloads.adblockplus.org/easylistitaly-minified.txt")!
        case .easylistRussia:
            return URL(string: "https://easylist-downloads.adblockplus.org/ruadlist-minified.txt")!
        case .easylistSpain:
            return URL(string: "https://easylist-downloads.adblockplus.org/easylistspanish-minified.txt")!
        case .easylistVietnam:
            return URL(string: "https://easylist-downloads.adblockplus.org/abpvn-minified.txt")!
        case .germanyCompliance:
            return URL(string: "https://raw.githubusercontent.com/abp-filters/abp-filters-compliance/main/germany.txt")!
        case .antiCV:
            return URL(string: "https://easylist-downloads.adblockplus.org/abp-filters-anti-cv.txt")!
        case .cocCoc:
            return URL(string: "https://browser.coccoc.com/js/ios_abp_custom_rules.txt")!
        }
    }
}

extension FilterSource {
    var storageFileName: String {
        switch self {
        case .easylist:
            return "easylist-minified"
        case .exceptions:
            return "exceptionrules-minimal"
        case .easylistChina:
            return "easylistChina-minified"
        case .easylistDutch:
            return "easylistDutch-minified"
        case .easylistFrance:
            return "easylistFrance-minified"
        case .easylistGermany:
            return "easylistGermany-minified"
        case .easylistItaly:
            return "easylistItaly-minified"
        case .easylistRussia:
            return "easylistRussia-minified"
        case .easylistSpain:
            return "easylistSpain-minified"
        case .easylistVietnam:
            return "easylistVietnam-minified"
        case .germanyCompliance:
            return "germanyCompliance"
        case .antiCV:
            return "antiCV"
        case .cocCoc:
            return "cocCoc"
        }
    }
}
