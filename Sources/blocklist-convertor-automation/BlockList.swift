
/// Describes the converted block lists
enum BlockList: CaseIterable {
    case easylist
    case easylistPlusExceptions

    case easylistChina
    case easylistChinaPlusExceptions

    case easylistDutch
    case easylistDutchPlusExceptions

    case easylistFrance
    case easylistFrancePlusExceptions

    case easylistGermany
    case easylistGermanyPlusExceptions

    case easylistItaly
    case easylistItalyPlusExceptions

    case easylistRussia
    case easylistRussiaPlusExceptions

    case easylistSpain
    case easylistSpainPlusExceptions

    case easylistVietnam
    case easylistVietnamPlusExceptions
}

extension BlockList {

    /// The filter lists from which a BlockList is composed
    var filterComposition: [FilterSource] {
        switch self {
        case .easylist:
            return [.easylist, .antiCV]
        case .easylistPlusExceptions:
            return [.easylist, .antiCV, .exceptions]
        case .easylistChina:
            return [.easylist, .antiCV, .easylistChina]
        case .easylistChinaPlusExceptions:
            return [.easylist, .antiCV, .easylistChina, .exceptions]
        case .easylistDutch:
            return [.easylist, .antiCV, .easylistDutch]
        case .easylistDutchPlusExceptions:
            return [.easylist, .antiCV, .easylistDutch, .exceptions]
        case .easylistFrance:
            return [.easylist, .antiCV, .easylistFrance]
        case .easylistFrancePlusExceptions:
            return [.easylist, .antiCV, .easylistFrance, .exceptions]
        case .easylistGermany:
            return [.easylist, .antiCV, .easylistGermany, .germanyCompliance]
        case .easylistGermanyPlusExceptions:
            return [.easylist, .antiCV, .easylistGermany, .germanyCompliance, .exceptions]
        case .easylistItaly:
            return [.easylist, .antiCV, .easylistItaly]
        case .easylistItalyPlusExceptions:
            return [.easylist, .antiCV, .easylistItaly, .exceptions]
        case .easylistRussia:
            return [.easylist, .antiCV, .easylistRussia]
        case .easylistRussiaPlusExceptions:
            return [.easylist, .antiCV, .easylistRussia, .exceptions]
        case .easylistSpain:
            return [.easylist, .antiCV, .easylistSpain]
        case .easylistSpainPlusExceptions:
            return [.easylist, .antiCV, .easylistSpain, .exceptions]
        case .easylistVietnam:
            return [.easylist, .antiCV, .easylistVietnam, .cocCoc]
        case .easylistVietnamPlusExceptions:
            return [.easylist, .antiCV, .easylistVietnam, .cocCoc, .exceptions]
        }
    }
}

extension BlockList {
    /// The safari versions that the list needs to be generated for
    var safariVersions: [Version] {
        return [Version("14.9"), Version("15.0")]
    }
}

extension BlockList {

    /// The cache file name to be used for storage
    /// - Important: It is important that these filenames are synced with https://gitlab.com/eyeo/filterlists/contentblockerlists,
    ///              Since final BlockLists will be deployed to the specified repo, in order to have hust the blocklist updated
    ///              the names should match. Also this name will be used to access the file from https://easylist-downloads.adblockplus.org/
    func storageFileName(_ safariVersion: Version) -> String {
        if safariVersion >= Version("15.0") {
            switch self {
            case .easylist:
                return "easylist_min_content_blocker+safari15"
            case .easylistPlusExceptions:
                return "easylist_min+exceptionrules_content_blocker+safari15"
            case .easylistChina:
                return "easylist+easylistchina-minified+safari15"
            case .easylistChinaPlusExceptions:
                return "easylist+easylistchina-minified+exceptionrules-minimal+safari15"
            case .easylistDutch:
                return "easylist+easylistdutch-minified+safari15"
            case .easylistDutchPlusExceptions:
                return "easylist+easylistdutch-minified+exceptionrules-minimal+safari15"
            case .easylistFrance:
                return "easylist+liste_fr-minified+safari15"
            case .easylistFrancePlusExceptions:
                return "easylist+liste_fr-minified+exceptionrules-minimal+safari15"
            case .easylistGermany:
                return "easylist+easylistgermany-minified+safari15"
            case .easylistGermanyPlusExceptions:
                return "easylist+easylistgermany-minified+exceptionrules-minimal+safari15"
            case .easylistItaly:
                return "easylist+easylistitaly-minified+safari15"
            case .easylistItalyPlusExceptions:
                return "easylist+easylistitaly-minified+exceptionrules-minimal+safari15"
            case .easylistRussia:
                return "easylist+ruadlist-minified+safari15"
            case .easylistRussiaPlusExceptions:
                return "easylist+ruadlist-minified+exceptionrules-minimal+safari15"
            case .easylistSpain:
                return "easylist+easylistspanish-minified+safari15"
            case .easylistSpainPlusExceptions:
                return "easylist+easylistspanish-minified+exceptionrules-minimal+safari15"
            case .easylistVietnam:
                return "easylist+abpvn-minified+safari15"
            case .easylistVietnamPlusExceptions:
                return "easylist+abpvn-minified+exceptionrules-minimal+safari15"
            }
        } else {
            switch self {
            case .easylist:
                return "easylist_min_content_blocker"
            case .easylistPlusExceptions:
                return "easylist_min+exceptionrules_content_blocker"
            case .easylistChina:
                return "easylist+easylistchina-minified"
            case .easylistChinaPlusExceptions:
                return "easylist+easylistchina-minified+exceptionrules-minimal"
            case .easylistDutch:
                return "easylist+easylistdutch-minified"
            case .easylistDutchPlusExceptions:
                return "easylist+easylistdutch-minified+exceptionrules-minimal"
            case .easylistFrance:
                return "easylist+liste_fr-minified"
            case .easylistFrancePlusExceptions:
                return "easylist+liste_fr-minified+exceptionrules-minimal"
            case .easylistGermany:
                return "easylist+easylistgermany-minified"
            case .easylistGermanyPlusExceptions:
                return "easylist+easylistgermany-minified+exceptionrules-minimal"
            case .easylistItaly:
                return "easylist+easylistitaly-minified"
            case .easylistItalyPlusExceptions:
                return "easylist+easylistitaly-minified+exceptionrules-minimal"
            case .easylistRussia:
                return "easylist+ruadlist-minified"
            case .easylistRussiaPlusExceptions:
                return "easylist+ruadlist-minified+exceptionrules-minimal"
            case .easylistSpain:
                return "easylist+easylistspanish-minified"
            case .easylistSpainPlusExceptions:
                return "easylist+easylistspanish-minified+exceptionrules-minimal"
            case .easylistVietnam:
                return "easylist+abpvn-minified"
            case .easylistVietnamPlusExceptions:
                return "easylist+abpvn-minified+exceptionrules-minimal"
            }
        }
    }
}

struct Version {
    // value should look like this: x.y.z whre x, y, z are non-negative integers
    let value: String
    init(_ value: String) {
        self.value = value
    }
}

extension Version: Equatable {}

extension Version: Comparable {
    static func < (lhs: Version, rhs: Version) -> Bool {
        var lhsComp = lhs.value.split(separator: ".").compactMap(Float.init)
        var rhsComp = rhs.value.split(separator: ".").compactMap(Float.init)
        
        // make the components have the same size to make comparisons trivial
        let lhsCount = lhsComp.count
        let rhsCount = rhsComp.count
        lhsComp.append(contentsOf: Array(repeating: 0, count: rhsCount))
        rhsComp.append(contentsOf: Array(repeating: 0, count: lhsCount))
        
        // zip and then compare first component that is different
        if let (l, r) = zip(lhsComp, rhsComp).first(where: { (l, r) in l != r }) {
            return l < r
        }
        
        return false
    }
}
