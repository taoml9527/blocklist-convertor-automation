import Foundation

enum Failure: Error {
    case filterListDownloadFailed(error: Error)
    case filterListCacheFailed
}

/// Downloads the latest version of base filters
/// Writes the results in Storage/DownloadedFilters
func downloadFilters() throws {

    print("ℹ️  Started filter lists download")
    print("\n")
    try FilterSource.allCases.forEach { source in
        let downloadURL = source.downloadURL

        print("⬇️  Downloading filter list for \(source) from \(downloadURL)")
        _ = try downloadFilterList(downloadURL).mapError(Failure.filterListDownloadFailed)
            .flatMap(cacheData(fileName: "\(source.storageFileName).txt"))
            .get()
        print("✅ Finished downloading filter list for \(source)")
        print("\n")
    }
}

// MARK: - IO

private func downloadFilterList(_ url: URL) -> Result<Data, Error> {
    Result { try Data(contentsOf: url) }
}

private func cacheData(fileName: String) -> (Data) -> Result<Void, Error> {
    return { data in
        let fullPath = downloadedFiltersFolder + fileName
        let isOK = FileManager.default.createFile(atPath: fullPath, contents: data)
        if isOK {
            return .success(())
        } else {
            return .failure(Failure.filterListCacheFailed)
        }
    }
}
