import Foundation

/// It is assumed that command line executable runs inside project directory
let projectRoot = FileManager.default.currentDirectoryPath

/// The storage meant to store latest succesfully deployed state
let storageFoolder = projectRoot + "/Storage/"

/// Will contain the downloaded source filters
let downloadedFiltersFolder = storageFoolder + "DownloadedFilters/"

/// Will contains merged source filters
let mergedFiltersFolders = storageFoolder + "MergedFilters/"

/// Submodule to which converted filters are to be deployed
let convertedBlockingRulesPath = storageFoolder + "ConvertedBlockingRules/"
